# Utilities for the runjs build tool.

- [What is runjs ?](https://github.com/pawelgalazka/runjs)
- [Get started](#get-started)
- [Define a task](#define-a-task)
- [Pass arguments through](#pass-arguments-through)

## Get started
Install runjs and runjs-util in your project:

```bash
npm install runjs @jedfong/runjs-util --save-dev
```

Create a `runfile.js` in your root project directory. The `buildTasks` function will import all tasks from the task directory.

```js
import { run } from 'runjs';
import { buildTasks } from '@jedfong/runjs-util';

export default buildTasks(run, __dirname);
```

## Define a task
Below is an example of a task file. The named export is important, as it becomes the task name.

```js
import { buildArgs } from 'runjs-util';

export const lint = ({ run, cwd }) => (opts = {}, ...params) => {
  // * run is s specially wrapped version of the run function defined by runjs.
  //     * defaulted to runfile.js cwd
  //     * if `--dry` is passed to the task, run will not execute anything, but will simply console.log the command
  //     * if the command to run is an array, this array will be flattened and joined with ' '
  // * cwd is the directory where runfile.js lives
  // * opts are the options normally found in runjs using `this.options`
  //     * note that `this` is no longer needed
  // * params are the parameters normally passed to the runjs task function

  const args = buildArgs({ cache: true, ...opts });
  const fileNames = params.length > 0 ? params : ['src'];
  run(['eslint', args, fileNames], { async: true });
};

lint.description = 'Run eslint against ./client';
lint.options = [
  { identifiers: 'fix', descriptions: 'Automatically fix problems [default: false]' },
];

```

## Pass arguments through
You may have noticed the `buildArgs` function. This helps make command-line tools easier to use when wrapped with runjs (eslint and jest are good examples of this). By utilizing this function, many of the options can be passed directly through to the command-line tool. Below is an example of how arguments are built by default:

```js
const args = buildArgs({
  a: true,
  b: 'true',
  c: false,
  d: 'false',
  e: 'f',
  foo: true,
  bar: 'true',
  baz: false
  qux: 'false',
  quux: 'quuz'
});

console.log(args.join(' ')));

// -a -b -e f --foo --bar --quux quuz
```

An argument handler can be overridden:
```js
const opts = {
  quux: 'quuz'
  foo: 'bar',
};
const handlers = {
  foo: (key, value) => `--${key} ${value.toUpperCase()}`;
};
const args = buildArgs(opts, { handlers });

console.log(args.join(' ')));

// --quux quuz --foo BAR
```



