module.exports = {
  plugins: [
    '@babel/plugin-syntax-dynamic-import',
  ],
  presets: [
    ['@babel/preset-env', {
      targets: {
        node: '8.9.4',
      },
    }],
  ],
};
