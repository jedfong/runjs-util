"use strict";

const EOL = require('os').EOL;

const {
  readdirSync
} = require('fs');

const {
  join
} = require('path');

const newlineIndent = (num = 1) => EOL + '  '.repeat(num);

const asArray = val => Array.isArray(val) ? val : [val];

const withDashes = options => {
  const dashedOptions = options.map(({
    identifiers = [],
    valueFormat = '',
    descriptions = []
  }) => {
    const valueFormatStr = valueFormat.trim() ? `=${valueFormat.trim()}` : '';
    const sortedIdentifiers = asArray(identifiers).map(i => i && i.trim()).filter(i => i && i.length > 0).sort((a, b) => a.length - b.length || a.localeCompare(b));
    const identifiersStr = sortedIdentifiers.map(i => i.length === 1 ? `-${i}` : `--${i}`).join(', ') + valueFormatStr;
    return {
      identifiers: identifiersStr,
      identifiersSort: sortedIdentifiers[0],
      descriptions: asArray(descriptions).filter(desc => desc)
    };
  });
  return dashedOptions.sort((a, b) => a.identifiersSort.localeCompare(b.identifiersSort));
};

const combineDuplicates = options => {
  return options.reduce((acc, {
    identifiers,
    descriptions
  }) => {
    const previousOption = acc[acc.length - 1];

    if (previousOption && previousOption.identifiers === identifiers) {
      const uniqeDescriptions = [...new Set([...previousOption.descriptions, ...descriptions])];
      previousOption.descriptions = uniqeDescriptions;
    } else {
      const uniqeDescriptions = [...new Set(descriptions)];
      acc = [...acc, {
        identifiers,
        descriptions: uniqeDescriptions
      }];
    }

    return acc;
  }, []);
};

const buildHelp = (taskDescription = '', options = []) => {
  let help = `${taskDescription + EOL + EOL}OPTIONS${newlineIndent(1)}`;
  const optionsWithDashes = combineDuplicates(withDashes(options));

  if (optionsWithDashes.length > 0) {
    help += optionsWithDashes.map(({
      identifiers,
      descriptions = []
    }) => {
      if (descriptions.length > 0) {
        return identifiers + newlineIndent(3) + descriptions.join(newlineIndent(3));
      } else {
        return identifiers;
      }
    }).join(EOL + newlineIndent(1));
  } else {
    help += 'None Available';
  }

  return help + EOL;
};

const flatten = arr => {
  return arr.reduce((acc, item) => {
    return [...acc, ...(Array.isArray(item) ? flatten(item) : [item])];
  }, []);
};

const dryRun = (cmd, opts) => console.log(cmd, JSON.stringify(opts || {})); // eslint-disable-line no-console


const wrapForCwd = (run, cwd) => (cmd, opts = {}) => {
  if (Array.isArray(cmd)) {
    return run(flatten(cmd).filter(i => i).join(' '), {
      cwd,
      ...opts
    });
  } else {
    return run(cmd, {
      cwd,
      ...opts
    });
  }
};

const getTasksFromFile = (runFn, cwd, taskFilePath) => {
  return Object.entries(require(taskFilePath)).reduce((acc, [taskName, task]) => {
    const taskFn = function (...args) {
      const run = wrapForCwd(this.options.dry ? dryRun : runFn, cwd); // eslint-disable-line no-invalid-this

      const {
        dry,
        ...optsWithoutDry
      } = this.options; // eslint-disable-line no-invalid-this

      return task({
        run,
        cwd
      })(replaceBooleans(optsWithoutDry), ...args);
    };

    taskFn.help = buildHelp(task.description, [...(task.options || []), {
      identifiers: 'dry',
      descriptions: 'Dry-run the task (no execution) [default: false]'
    }]);
    return { ...acc,
      [taskName]: taskFn
    };
  }, {});
};

const replaceBooleans = opts => Object.entries(opts).reduce((acc, [key, value]) => {
  if (value === 'false') {
    acc[key] = false;
  } else if (value === 'true') {
    acc[key] = true;
  } else {
    acc[key] = value;
  }

  return acc;
}, {});

const buildTasks = (run, cwd, {
  tasksDir
} = {}) => {
  tasksDir = tasksDir || join(cwd, 'tasks');
  return readdirSync(tasksDir).reduce((acc, taskFileName) => {
    return { ...acc,
      ...getTasksFromFile(run, cwd, join(tasksDir, taskFileName))
    };
  }, {});
};

const defaultHandler = (key, value) => {
  const prefix = key.length === 1 ? '-' : '--';

  if (value === false || value === 'false') {
    return `${prefix}${key}=false`;
  } else if (value === true || value === 'true') {
    return `${prefix}${key}`;
  } else {
    return `${prefix}${key}=${value}`;
  }
};

const buildArgs = (opts = {}, handlers = {}) => {
  return Object.entries(opts).map(([key, value]) => {
    const handler = handlers[key] || defaultHandler;
    return handler(key, value);
  }).filter(arg => arg);
};

module.exports = {
  buildTasks,
  buildArgs
};